package com.vodafone.orders.service;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import com.vodafone.orders.dao.OrderJPARepository;
import com.vodafone.orders.dto.OrderDetails;
import com.vodafone.orders.event.OrderEvent;
import com.vodafone.orders.event.OrderStatus;
import com.vodafone.orders.exception.InvalidOrderIdException;
import com.vodafone.orders.model.Order;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderService {

	private final OrderJPARepository orderRepository;
	private final WebClient webClient;
	
	@CircuitBreaker(name="inventoryservice", fallbackMethod = "fallBack")
	@Transactional
	public Order saveOrder(Order order) {
		Order savedOrder = this.orderRepository.save(order);
		//will invoke a post request to inventory microservice
		/*
		 * this.webClient.post() .uri("/api/inventory") .exchangeToMono(res ->
		 * res.bodyToMono(Long.class)) .block();
		 */
		OrderEvent orderEvent = new OrderEvent(savedOrder, OrderStatus.ORDER_ACCEPTED, LocalDateTime.now());
		return savedOrder;
	}

	private Order fallBack(Exception exception) {
		System.out.println("Exception while calling the inventory microservice");
		return Order.builder().build();
	}
	public Map<String, Object> fetchAllOrder(int page, int size, String sortDirection, String property) {
		Sort.Direction direction = sortDirection.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
		Pageable pageRequest = PageRequest.of(page, size, direction, property);
		Page<OrderDetails> pageResponse = this.orderRepository.findBy(pageRequest);

		long totalRecords = pageResponse.getTotalElements();
		int pages = pageResponse.getTotalPages();
		int currentPage = pageResponse.getNumber();
		List<OrderDetails> data = pageResponse.getContent();

		Map<String, Object> responseMap = new LinkedHashMap<>();
		responseMap.put("total", totalRecords);
		responseMap.put("pages", pages);
		responseMap.put("page", currentPage);
		responseMap.put("content", data);
		return responseMap;
	}

	public Order fetchOrderById(long id) throws InvalidOrderIdException {
		if (id < 3000) {
			Optional<Order> optionalOrder = this.orderRepository.findById(id);
			Order fetchedOrder = optionalOrder.orElseThrow(() -> new InvalidOrderIdException("invalid order id passed"));
			return fetchedOrder;
		} else {
			return null;
		}
	}

	public Order updateOrderById(long id, Order updatedOrder) throws InvalidOrderIdException {
		Order orderFromDB = this.fetchOrderById(id);
		orderFromDB.setCustomerEmail(updatedOrder.getCustomerEmail());
		orderFromDB.setDate(updatedOrder.getDate());
		orderFromDB.setPrice(updatedOrder.getPrice());
		orderFromDB.setCustomerName(updatedOrder.getCustomerName());
		Order savedOrder = this.orderRepository.save(orderFromDB);
		return savedOrder;
	}

	public Set<Order> findOrdersByPriceBetween(double minPrice, double maxPrice) {
		return this.orderRepository.findByPriceBetween(minPrice, maxPrice);
	}

	public Map<String, Object> findOrdersByPriceMax(double maxPrice, int page, int size) {
		Pageable pageRequest = PageRequest.of(page, size);
		Page<Order> pageResponse = this.orderRepository.findByPriceLessThan(maxPrice, pageRequest);

		long totalRecords = pageResponse.getTotalElements();
		int pages = pageResponse.getTotalPages();
		int currentPage = pageResponse.getNumber();
		List<Order> data = pageResponse.getContent();

		Map<String, Object> responseMap = new LinkedHashMap<>();
		responseMap.put("total", totalRecords);
		responseMap.put("pages", pages);
		responseMap.put("page", currentPage);
		responseMap.put("content", data);
		return responseMap;
	}

	public void deleteOrderById(long orderId) {
		this.orderRepository.findById(orderId).ifPresent(this.orderRepository::delete);
	}
}
package com.vodafone.orders.model;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PastOrPresent;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 * Entity
 *    - has representation in the Database
 *    - objects will correspond to the records in the table
 *    - properities will correspond to the columns in the table
 *    - Managed by Hibernate/JPA
 */
@Data
@NoArgsConstructor

@Entity
@Table(name="orders")
@Builder
@AllArgsConstructor
public class Order {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@JsonProperty(value = "customer_name")
	@NotEmpty(message = "customer name cannot be empty")
	private String customerName;

	@JsonProperty(value = "customer_email")
	@Email(message = "email is not in the right format")
	@NotEmpty(message = "customer email cannot be empty")
	private String customerEmail;
	
	@JsonProperty(value = "price")
	@Min(value = 400, message = "Minimum order price should be 400")
	private double price;
	
	@JsonFormat(pattern = "dd-MM-yyyy")
	@PastOrPresent(message="Order date cannot be in the future")
	private LocalDate date;
	
	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	private Set<LineItem> lineItems;
	
	//scaffolding code
	public void addLineItem(LineItem lineItem) {
		if (this.lineItems == null) {
			this.lineItems = new HashSet<>();
		}
		this.lineItems.add(lineItem);
		lineItem.setOrder(this);
	}

	//generate constructor, setter/getter, toString, equals, hashcode
	
}

package com.vodafone.orders.exception;

import static java.util.stream.Collectors.toSet;

import java.util.Set;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
	
	@ExceptionHandler(InvalidOrderIdException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public Error handleGlobalException(InvalidOrderIdException exception) {
		return new Error(100, exception.getMessage());
	}
	
	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public Error handleRuntimeException(InvalidOrderIdException exception) {
		return new Error(100, exception.getMessage());
	}
	
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public Set<String> handleRuntimeException(MethodArgumentNotValidException exception) {
		log.error("Exception with the result");
		Set<String> messages = exception
									.getAllErrors()
									.stream()
									.map(ObjectError::getDefaultMessage)
									.collect(toSet());
		
		return messages;
	}
}

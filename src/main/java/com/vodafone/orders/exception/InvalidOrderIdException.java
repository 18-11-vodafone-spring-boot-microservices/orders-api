package com.vodafone.orders.exception;

public class InvalidOrderIdException extends Exception {
	
	public InvalidOrderIdException(String message) {
		super(message);
	}
	
	public String getMessage() {
		return super.getMessage();
	}

}

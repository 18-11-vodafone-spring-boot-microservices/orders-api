package com.vodafone.orders.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.vodafone.orders.dao.OrderJPARepository;

import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
class DBHealthIndicator implements HealthIndicator {

	private final OrderJPARepository orderRepository;

	@Override
	public Health health() {
		long count = this.orderRepository.count();
		if (count >= 0) {
			return Health.up().withDetail("DB", "db service is running").build();
		}
		return Health.down().withDetail("DB", "db service is down").build();
	}
}

@Configuration
@RequiredArgsConstructor
class KafkaHealthIndicator implements HealthIndicator {

	@Override
	public Health health() {
		return Health.up().withDetail("Kafka", "Kafka service is running").build();

	}
}

@Configuration
@RequiredArgsConstructor
class PaymentHealthIndicator implements HealthIndicator {

	private final RestTemplate restTemplate;
	private static final String APP_URL = "https://jsonplaceholder.typicode.com/users";

	@Override
	public Health health() {
		try {
			ResponseEntity<String> responseEntity = this.restTemplate.getForEntity(APP_URL, String.class);
			System.out.println(responseEntity.getBody());
			return Health.up().withDetail("User-Service", "User service is running").build();

		} catch (Exception exception) {
			return Health.down().withDetail("User-Service", "User-Service service is down").build();
		}

	}
}
package com.vodafone.orders.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class ApplicationConfig {

	@Bean
	public User user() {
		// you have control on the creation of the object
		return new User();
	}

	@ConditionalOnProperty(prefix = "app", name = "loadUser", havingValue = "true", matchIfMissing = true)
	@Bean
	public User userBasedOnPropertyCondition() {
		User user = new User();
		return user;
	}

	@ConditionalOnBean(name = "user")
	@Bean
	public User userBasedOnBean() {
		User user = new User();
		return user;
	}

	@ConditionalOnMissingBean(name = "user")
	@Bean
	public User userBasedOnMissingBean() {
		User user = new User();
		return user;
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean 
	public WebClient webClient(){
		return WebClient
			.builder()
			//svc name should be configured
			.baseUrl("http://localhost:9222")
			.build();

	}
}

package com.vodafone.orders.controller;

import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.vodafone.orders.exception.InvalidOrderIdException;
import com.vodafone.orders.model.Order;
import com.vodafone.orders.service.OrderService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
@Slf4j
public class OrderRestController {
	
	private final OrderService orderService;
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Order saveOrder(@RequestBody @Valid Order order) {
		return this.orderService.saveOrder(order);
	}
	
	@GetMapping
	public Map<String, Object> fetchAllOrders(
			@RequestParam(name = "page", defaultValue = "0", required = false) int page,
			@RequestParam(name = "records", defaultValue = "10", required = false) int size,
			@RequestParam(name = "sort", defaultValue = "asc", required = false) String direction,
			@RequestParam(name = "field", defaultValue = "customerName", required = false) String property ){
		return this.orderService.fetchAllOrder(page, size, direction, property);
	}
	
	@GetMapping("/{id}")
	public Order fetchOrderByOrderId(@PathVariable long id) throws InvalidOrderIdException{
		return this.orderService.fetchOrderById(id);
	}
	
	@GetMapping("/price")
	public Set<Order> fetchOrderByPriceRange(
			@RequestParam(name = "min", defaultValue = "4000", required = false) double min,
			@RequestParam(name = "max", defaultValue = "4002", required = false) double max
			){
		log.info("Inside the rest controller to execure by price range , {} {}", min, max);
		return this.orderService.findOrdersByPriceBetween(min, max);
	}

	@GetMapping("/price/{amount}")
	public Map<String, Object> fetchOrderByPriceLessThan(
			@PathVariable double amount,
			@RequestParam(name = "page", defaultValue = "0", required = false) int page,
			@RequestParam(name = "records", defaultValue = "10", required = false) int size
			){
		return this.orderService.findOrdersByPriceMax(amount, page, size);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteOrderById(@PathVariable long id) {
		this.orderService.deleteOrderById(id);
	}

}

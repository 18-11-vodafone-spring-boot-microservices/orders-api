package com.vodafone.orders.dao;

import java.time.LocalDate;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.vodafone.orders.dto.OrderDetails;
import com.vodafone.orders.model.Order;

@Repository
public interface OrderJPARepository extends JpaRepository<Order, Long> {

	Set<Order> findByPriceBetween(double min, double max);

	Set<Order> findByDateBetween(LocalDate startDate, LocalDate endDate);

	Page<Order> findByPriceLessThan(double price, Pageable page);

	@Query("select o from Order o where o.customerEmail = ?1 ")
	Set<Order> findAllOrdersByEmailAddress(String emailAddress);
	
	Page<OrderDetails> findBy(Pageable pageRequest);
}

package com.vodafone.orders.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;

public interface OrderDetails {

	@JsonProperty("customer_name")
	String getCustomerName();

	@JsonProperty("customer_email")
	String getCustomerEmail();

	double getPrice();

	LocalDate getDate();
}

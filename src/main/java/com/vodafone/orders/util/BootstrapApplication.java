package com.vodafone.orders.util;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

import com.vodafone.orders.config.User;

@Configuration
public class BootstrapApplication implements CommandLineRunner {
	
	//@Autowired
	private ApplicationContext applicationContext;
	
	//@Autowired
	private User user;
	
	public BootstrapApplication(ApplicationContext applicationContext, User user) {
		this.applicationContext = applicationContext;
		this.user = user;
	}
	

	@Override
	public void run(String... args) throws Exception {
		System.out.println ("Hello world from Spring Boot !!");
		String[] beans = this.applicationContext.getBeanDefinitionNames();
		
		
		//for(String bean: beans) { System.out.println("Bean : "+ bean); }
		Arrays.asList(beans)
			  .stream()
			  .filter(bean -> bean.startsWith("user"))
			  .forEach(System.out::println);
	}
}

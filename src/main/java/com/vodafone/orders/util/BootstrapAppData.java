package com.vodafone.orders.util;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;
import com.vodafone.orders.dao.OrderJPARepository;
import com.vodafone.orders.model.LineItem;
import com.vodafone.orders.model.Order;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@RequiredArgsConstructor
public class BootstrapAppData {
	private final Faker faker = new Faker();
	private final OrderJPARepository orderRepository;
	
	@Value("${app.orderCount}")
	private int orderCount;

	@EventListener(ApplicationReadyEvent.class)
	public void bootstrapData(ApplicationReadyEvent appReadyEvent) {
		log.info("Inserting data for testing....");
		/*
		 * Order order = new Order(12, 233, true, true, "Sachin" ,"Krishna", false);
		 * Order order = new Order(12, 233, "Sachin" ,"Krishna");
		 * 
		 */
		IntStream.range(0, orderCount).forEach(index -> {
			String firstName = faker.name().firstName();

			Order order = Order.builder().customerEmail(firstName + "@" + faker.internet().domainName())
					.customerName(firstName)
					.date(faker.date().past(4, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
					.price(faker.number().randomDouble(2, 400, 5000)).build();
			
			IntStream.range(0, faker.number().numberBetween(2, 5)).forEach(value -> {
				LineItem lineItem = LineItem.builder().name(faker.commerce().productName())
													.price(faker.number().randomDouble(2, 400, 600))
													.qty(faker.number().numberBetween(2, 6))
													.build();
				order.addLineItem(lineItem);
			});
			double totalOrderPrice = order
										.getLineItems()
										.stream()
										.map(lineItem -> lineItem.getQty() * lineItem.getPrice())
										.reduce(Double::sum)
										.orElse(0d);
			order.setPrice(totalOrderPrice);
			this.orderRepository.save(order);
		});
	}

}

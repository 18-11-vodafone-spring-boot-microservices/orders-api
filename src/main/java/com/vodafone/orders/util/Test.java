package com.vodafone.orders.util;

import java.util.HashSet;
import java.util.Set;

import com.vodafone.orders.model.LineItem;
import com.vodafone.orders.model.Order;

public class Test {
	public static void main(String[] args) {
		LineItem lineItem = LineItem.builder().id(12).name("test").price(34).qty(2).build();
		Set<LineItem> items = new HashSet<>();
		items.add(lineItem);
		Order o = Order.builder().id(23L).customerEmail("dsfdf").customerName("test").build();
		lineItem.setOrder(o);
		o.setLineItems(items);
	
		
		System.out.println(o);
	}

}

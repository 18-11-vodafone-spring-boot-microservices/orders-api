package com.vodafone.orders.event;

import java.time.LocalDateTime;

import com.vodafone.orders.model.Order;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class OrderEvent {

	private Order order;
	private OrderStatus status;
	private LocalDateTime timestamp;

}

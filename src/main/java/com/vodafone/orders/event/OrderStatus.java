package com.vodafone.orders.event;

public enum OrderStatus {
	ORDER_ACCEPTED,
	ORDER_DECLINED,
	ORDER_PENDING

}

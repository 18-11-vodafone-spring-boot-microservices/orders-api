package com.vodafone.orders.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/*
 * Template for writing the test case
 * 1. Execute the code under the test
 * 2. Assert the hypothesis
 */
public class OrderTests {

	@Test
	void testConstructor() {

		Order order = Order.builder()
							.customerEmail("user@gmail.com")
							.customerName("Vinay")
							.date(LocalDate.now())
							.price(2500).build();
		
		assertNotNull(order);
		assertEquals("user@gmail.com", order.getCustomerEmail());
		assertEquals("Vinay", order.getCustomerName());
		assertEquals(LocalDate.now(), order.getDate());
	}
	
	@Test
	void testSetter() {
		Order order = Order.builder().build();
		order.setCustomerEmail("user@gmail.com");
		order.setCustomerName("Vinay");
		Assertions.assertNotNull(order);
		assertEquals("user@gmail.com", order.getCustomerEmail());
		assertEquals("Vinay", order.getCustomerName());
	}

}

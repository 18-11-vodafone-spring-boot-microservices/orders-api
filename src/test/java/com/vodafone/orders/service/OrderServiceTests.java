package com.vodafone.orders.service;

import static java.util.Optional.ofNullable;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import com.vodafone.orders.dao.OrderJPARepository;
import com.vodafone.orders.exception.InvalidOrderIdException;
import com.vodafone.orders.model.Order;

@ExtendWith(MockitoExtension.class)
/**
 * 
 * Sequence of operation 1. Create a mock object 2. Set the expectations on the
 * Mock object 3. Execute the method under test 4. Assert the hypothesis 5.
 * Verify the expectations - DONT IGNORE
 */
public class OrderServiceTests {

	@InjectMocks
	private OrderService orderService;

	@Mock
	private OrderJPARepository orderRepository;

	@Test
	void testSaveOrder() {
		Order order = Order.builder().customerEmail("user@gmail.com").customerName("Vinay").date(LocalDate.now())
				.price(2500).build();

		Order order1 = Order.builder().id(12L).customerEmail("user@gmail.com").customerName("Vinay")
				.date(LocalDate.now()).price(2500).build();
		// set the expectations on the mock object
		when(this.orderRepository.save(any(Order.class))).thenReturn(order1);

		// execute the method
		Order savedOrder = this.orderService.saveOrder(order);

		assertNotNull(savedOrder);
		assertEquals(12L, savedOrder.getId());
		assertEquals(savedOrder, order1);

		// Verify the expectations
		verify(this.orderRepository, times(1)).save(order);
	}

	@Test
	void validOrderIdFetch() throws InvalidOrderIdException {
		Order order = Order.builder().id(2000L).customerEmail("user@gmail.com").customerName("Vinay")
				.date(LocalDate.now()).price(2500).build();

		// set the expectations on the mock object
		when(this.orderRepository.findById(anyLong())).thenReturn(ofNullable(order));

		// execute the method
		Order orderFromDB = this.orderService.fetchOrderById(2000);

		assertNotNull(orderFromDB);
		assertEquals(2000L, orderFromDB.getId());
		assertEquals(orderFromDB, order);

		// Verify the expectations
		verify(this.orderRepository, times(1)).findById(2000L);
	}

	@Test
	void invalidOrderIdFetch() {
		// set the expectations on the mock object
		when(this.orderRepository.findById(anyLong())).thenReturn(ofNullable(null));

		// execute the method
		assertThrows(InvalidOrderIdException.class, () -> orderService.fetchOrderById(2000));

		// Verify the expectations
		verify(this.orderRepository, times(1)).findById(2000L);
	}
	
	@Test
	void invalidOrderIdFetch2() throws InvalidOrderIdException {
		// set the expectations on the mock object
		lenient().when(this.orderRepository.findById(anyLong())).thenReturn(ofNullable(null));
		// execute the method
		Order response = orderService.fetchOrderById(4000);
		assertNull(response);
		// Verify the expectations
		verify(this.orderRepository, times(0)).findById(4000L);
	}
}

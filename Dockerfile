FROM openjdk:11-jdk-slim as builder

# set up a working directory
WORKDIR /app

# copy the dependencies
# COPY files from source to destination
COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .

# install the dependencies
RUN chmod +x ./mvnw && ./mvnw -B dependency:go-offline

COPY src src

RUN ./mvnw package -DskipTests

RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)
# completes the build stage


# create a brand new image from the JRE image

FROM openjdk:11.0.13-jre-slim-buster as stage

ARG DEPENDENCY=/app/target/dependency

COPY --from=builder ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=builder ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=builder ${DEPENDENCY}/BOOT-INF/classes /app

# debugging with curl
RUN apt update && apt install -y curl

EXPOSE 8222

ENTRYPOINT ["java", "-cp", "app:app/lib/*", "com.vodafone.orders.OrdersApiApplication"]